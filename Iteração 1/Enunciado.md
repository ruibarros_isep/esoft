Um centro de exposições do norte do país pretende desenvolver uma aplicação (de software) de apoio à organização e gestão de exposições que resolva problemas relacionados com a introdução da informação por diferentes funcionários (até agora a informação é preenchida várias vezes e não há garantias de correção entre os vários formulários e informação já existente).

Entre outras coisas, este software deve permitir registar as várias exposições que irão realizar-se ao longo do ano, bem como facilitar o processo de atribuição de ocupação dos espaços de exposição (stands) aos respetivos expositores que irão estar presentes nas várias exposições.

Ao criar uma exposição, o gestor de exposições define o seu título, um texto descritivo sobre o âmbito da mesma, o período e o local de realização, e um conjunto de pessoas responsáveis pela sua realização (os organizadores). Numa fase posterior à criação de uma exposição no sistema, um dos organizadores define quais os funcionários de apoio à exposição (FAE). Os FAE são as pessoas que farão posteriormente, além de outras tarefas, decide quais os expositores selecionados para a exposição.

Aquando da candidatura a uma exposição, o representante do expositor (empresa) deve especificar o nome comercial da empresa, morada, telemóvel, a área de exposição pretendida, os produtos a expor e a quantidade de convites a adquirir.
Um dos organizadores atribui aos FAE as candidaturas dos expositores para decisão. Para cada candidatura, os FAE indicam no sistema se esta foi aceite ou recusada, juntamente com um breve texto justificativo.

É necessário que todos os gestores de exposições, organizadores, FAE e expositores estejam registados no sistema como utilizadores. Para tal, o utilizador não registado cria o seu perfil de utilizador indicando o nome, endereço de email e credenciais de acesso (password é armazenada cifrada). Um gestor de exposições confirma posteriormente o registo.

O núcleo principal do software deve ser implementado em Java. Com o intuito de aumentar a manutenibilidade do software, devem ser adotadas boas práticas de análise e design de software OO.