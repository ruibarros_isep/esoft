Registo de utilizador: Processo através do qual uma pessoa solicita o acesso ao sistema por forma a operar sobre o sistema (e.g. autor).

Utilizador: Representa uma pessoa que utiliza o sistema.

Utilizador não registado: Representa uma pessoa que utiliza o sistema anonimamente e se pode registar.

